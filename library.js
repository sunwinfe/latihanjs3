class Hewan {
	constructor(nama, jenis) {
		this.nama = nama;
		this.jenis = null;
	}
}

// OBJECT, CLASS, CONSTRUCTOR, SETTER & GETTER
class Animal {
	constructor(nama, jenis) {
		this.nama = nama;
		this._jenis = null;
	}

	//template literals pada es6 mengunakan karakter backtick `` memasukkan nilai ke dalam string menjadi lebih mudah menggunakan sintaks ${}.
	get jenis() {
		return `Jenis, hewannya adalah ${this._jenis}`;
	}

	set jenis(jenis) {
		this._jenis = `berkembang biak dengan ${jenis} `;
	}
}
// KONSEP ABSTRAKSI

function Jumlah(jmlh) {
	var jmlh = jmlh;
	var totalBiaya = function () {
		return jmlh * 14000;
	};

	this.tampilBiaya = function () {
		return totalBiaya();
	};
}
// KONSEP INHERITANCE
// super class / parent class
class Vehicle {
	constructor(licensePlate, manufacture) {
		this.licensePlate = licensePlate;
		this.manufacture = manufacture;
	}

	info() {
		// template literals pada es6 mengunakan karakter backtick `` memasukkan nilai ke dalam string menjadi lebih mudah menggunakan sintaks ${}.
		console.log(`Nomor Kendaraan : ${this.licensePlate}`);
		console.log(`Manufacture: ${this.manufacture}`);
	}

	parking() {
		console.log(`Kendaraan ${this.licensePlate} parkir!`);
	}
}

// child class inherit properties & method from parent class by keyword 'extends'
class Car extends Vehicle {
	constructor(licensePlate, manufacture, wheels) {
		super(licensePlate, manufacture);
		this.wheels = wheels;
	}
	droveOff() {
		console.log(`Kendaraan ${this.licensePlate} melaju!`);
	}

	openDoor() {
		console.log('Membuka Pintu! ');
	}

	/*overriding method info dari parent class */
	info() {
		super.info();
		console.log(`Jumlah roda: ${this.wheels}`);
	}
}

// KONSEP POLYMORPHISM
class Transporasi {
	constructor(name) {
		this.name = name;
	}
	jalur() {
		console.log(`${this.name} pergi dengan transportasi darat`);
	}
}

class Persons extends Transporasi {
	constructor(name) {
		super(name);
	}
	// jalur() {
	// 	console.log(`${this.name} pergi dengan transporasti udara `);
	// }
}
